#ifndef I2CMODULE_H
#define I2CMODULE_H

#include <Arduino.h>

class I2CModule {
	private:
		static const unsigned int DELAY_MICROSECONDS = 0;
		byte sdaPin;
		byte sclPin;
		void setPin(byte, bool);
		void writeBit(bool);
	public:
		static const bool WRITE = false;
		static const bool READ = true;
		I2CModule(byte, byte);
		I2CModule();
		void start(byte, bool);
		void stop();
		void transfer(byte, bool);
};

#endif