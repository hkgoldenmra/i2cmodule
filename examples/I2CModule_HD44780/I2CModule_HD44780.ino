#include <I2CModule.h>

const byte sda = A4;
const byte scl = A5;

I2CModule i2cModule = I2CModule(sda, scl);

void setup() {
	// start I2C pairing with address = 0x27 = B00100111
	i2cModule.start(0x27, I2CModule::WRITE);

	// 4-bit mode = 0x20 = B00100000
	i2cModule.transfer(0x2C, I2CModule::WRITE);
	i2cModule.transfer(0x28, I2CModule::WRITE);
	i2cModule.transfer(0x0C, I2CModule::WRITE);
	i2cModule.transfer(0x08, I2CModule::WRITE);

	// 4-bit mode + 2 rows + 5x8 dots = 0x28 = B00101000
	i2cModule.transfer(0x2C, I2CModule::WRITE);
	i2cModule.transfer(0x28, I2CModule::WRITE);
	i2cModule.transfer(0x8C, I2CModule::WRITE);
	i2cModule.transfer(0x88, I2CModule::WRITE);

	// screen on + cursor on + blink on = 0x0F = B00001111
	i2cModule.transfer(0x0C, I2CModule::WRITE);
	i2cModule.transfer(0x08, I2CModule::WRITE);
	i2cModule.transfer(0xFC, I2CModule::WRITE);
	i2cModule.transfer(0xF8, I2CModule::WRITE);

	// clear = 0x01 = B00000001
	i2cModule.transfer(0x0C, I2CModule::WRITE);
	i2cModule.transfer(0x08, I2CModule::WRITE);
	i2cModule.transfer(0x1C, I2CModule::WRITE);
	i2cModule.transfer(0x18, I2CModule::WRITE);

	// "A" = 0x41 = B01000001
	i2cModule.transfer(0x4D, I2CModule::WRITE);
	i2cModule.transfer(0x49, I2CModule::WRITE);
	i2cModule.transfer(0x1D, I2CModule::WRITE);
	i2cModule.transfer(0x19, I2CModule::WRITE);

	// stop I2C pairing
	i2cModule.stop();
}

void loop() {
	delay(1000);
}