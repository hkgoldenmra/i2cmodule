#include <I2CModule.h>

void I2CModule::setPin(byte pin, bool data) {
	pinMode(pin, OUTPUT);
	digitalWrite(pin, data);
	delayMicroseconds(I2CModule::DELAY_MICROSECONDS);
}

void I2CModule::writeBit(bool data) {
	this->setPin(this->sclPin, LOW);
	this->setPin(this->sdaPin, data);
	this->setPin(this->sclPin, HIGH);
}

I2CModule::I2CModule(byte sdaPin, byte sclPin) {
	this->sdaPin = sdaPin;
	this->sclPin = sclPin;
}

I2CModule::I2CModule() :
I2CModule(SDA, SCL) {
}

void I2CModule::start(byte address, bool rw) {
	this->setPin(this->sclPin, HIGH);
	this->setPin(this->sdaPin, HIGH);
	this->setPin(this->sdaPin, LOW);
	this->transfer((address << 1) | rw, rw);
}

void I2CModule::stop() {
	this->setPin(this->sclPin, HIGH);
	this->setPin(this->sdaPin, LOW);
	this->setPin(this->sdaPin, HIGH);
}

void I2CModule::transfer(byte data, bool ack) {
	for (byte shift = 0x80; shift > 0; shift >>= 1) {
		this->writeBit((data & shift) > 0);
	}
	this->writeBit(ack);
}